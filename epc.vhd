-- Code belongs to EdJoPaTo

library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;


entity epc is
    generic (
        delayVisualization : time := 0 ns              -- visualization of just some HW delay
    );
    port (
		i1 : in std_logic;
        i2 : in std_logic;
        i3 : in std_logic;
        i4 : in std_logic;
        o  : out std_logic
    );
end entity epc;



architecture arc of epc is
begin
    
    logic:
    process ( i1, i2, i3, i4 ) is
		variable resu_v : std_logic;
    begin
		resu_v := not ((i1 xor i2) xor (i3 xor i4));
		
		o  <=  resu_v after delayVisualization;
    end process logic;
        
end architecture arc;
