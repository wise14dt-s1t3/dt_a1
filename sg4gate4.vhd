-- Code belongs to EdJoPaTo

library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;


entity sg4gate4 is
    port (
		w : out std_logic;
        x : out std_logic;
        y : out std_logic;
        z : out std_logic
    );--]port
end entity sg4gate4;


architecture beh of sg4gate4 is
begin

    sg:
    process
    begin
        
        w <= '0';  x <= '0';  y <= '0';  z <= '0';   wait for 10 ns;
        w <= '0';  x <= '0';  y <= '0';  z <= '1';   wait for 10 ns;
        w <= '0';  x <= '0';  y <= '1';  z <= '0';   wait for 10 ns;
        w <= '0';  x <= '0';  y <= '1';  z <= '1';   wait for 10 ns;
        w <= '0';  x <= '1';  y <= '0';  z <= '0';   wait for 10 ns;
        w <= '0';  x <= '1';  y <= '0';  z <= '1';   wait for 10 ns;
        w <= '0';  x <= '1';  y <= '1';  z <= '0';   wait for 10 ns;
        w <= '0';  x <= '1';  y <= '1';  z <= '1';   wait for 10 ns;
        w <= '1';  x <= '0';  y <= '0';  z <= '0';   wait for 10 ns;
        w <= '1';  x <= '0';  y <= '0';  z <= '1';   wait for 10 ns;
        w <= '1';  x <= '0';  y <= '1';  z <= '0';   wait for 10 ns;
        w <= '1';  x <= '0';  y <= '1';  z <= '1';   wait for 10 ns;
        w <= '1';  x <= '1';  y <= '0';  z <= '0';   wait for 10 ns;
        w <= '1';  x <= '1';  y <= '0';  z <= '1';   wait for 10 ns;
        w <= '1';  x <= '1';  y <= '1';  z <= '0';   wait for 10 ns;
        w <= '1';  x <= '1';  y <= '1';  z <= '1';   wait for 10 ns;
        
        wait;
    end process sg;
    
end architecture beh;
