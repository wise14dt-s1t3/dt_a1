-- Code belongs to EdJoPaTo

library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;


entity tb4epc is
end entity tb4epc;


architecture beh of tb4epc is
    
    signal i1_s : std_logic;
    signal i2_s : std_logic;
    signal i3_s : std_logic;
    signal i4_s : std_logic;
    signal o_s  : std_logic;
    
    
    
    component sg4gate4
        port(
			w : out std_logic;
			x : out std_logic;
			y : out std_logic;
			z : out std_logic
        );--]port
    end component sg4gate4;
    for all : sg4gate4 use entity work.sg4gate4( beh );
    
    component epc
        port(
			i1 : in std_logic;
			i2 : in std_logic;
			i3 : in std_logic;
			i4 : in std_logic;
			o : out std_logic
       );--]port
    end component epc;
    for all : epc use entity work.epc( arc );
    
begin
    
    sg_i : sg4gate4
        port map (
            w => i1_s,
			x => i2_s,
            y => i3_s,
			z => i4_s
        )--]port
    ;--]sg_i
    
    epc_i : epc
        port map (
            o  => o_s,
            i1 => i1_s,
            i2 => i2_s,
			i3 => i3_s,
			i4 => i4_s
        )--]port
    ;--]and2_i
    
end architecture beh;
