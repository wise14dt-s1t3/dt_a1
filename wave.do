onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider -height 20 In
add wave -noupdate /tb4epc/i1_s
add wave -noupdate /tb4epc/i2_s
add wave -noupdate /tb4epc/i3_s
add wave -noupdate /tb4epc/i4_s
add wave -noupdate -divider -height 20 Out
add wave -noupdate /tb4epc/epc_i/logic/resu_v
add wave -noupdate /tb4epc/o_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {154645 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {198256 ps}
